// program demonstracyjny dla ukladu ADP5585
// wyswietla na ekranie HD44780 eventy przyciskow
#include "stm32f0xx.h"
#include "DMB/rejestry.h"
#include "DMB/debug.h"
#include "RCC/RCC.h"
#include "delay_systick/delay_systick.h"

#include "HD44780/HD44780.h"
#include "TWI/twi.h"
#include "ADP5585/ADP5585.h"


void _delay(unsigned int x)
{
	while (x--);
}

void moja_funkcja_eventu( uint8_t event_ID )
{
	LCD_GoTo(0,1);
	LCD_WriteNumber( event_ID & 0x7f );
	LCD_WriteText(" ");
	if( event_ID & 0x80 )
	{
		LCD_WriteText("DOWN");
	}
	else
	{
		LCD_WriteText("UP");
	}
	LCD_WriteText("       ");
}

int main(void)
{
	RCC_init();
	delay_init();
	// wlaczenie zegara dla portu A i B
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;
	RCC->AHBENR |= RCC_AHBENR_GPIOBEN;

	_delay_ms( 1000 );


	LCD_Initalize();
	LCD_Clear();
	LCD_GoTo(0,0);
	LCD_WriteText("Przycisk:");

	_delay_ms(1000);

	ADP5585_init();
	ADP5585_register_event_callback( moja_funkcja_eventu );

	while(1)
	{
		ADP5585_event_check();
		__WFI();
	}
}

