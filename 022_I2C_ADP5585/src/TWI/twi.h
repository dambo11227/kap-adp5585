/*
* twi.h
*
*  Created on: 5 pa� 2016
*      Author: Przemek
*/

#ifndef TWI_H_
#define TWI_H_

#ifdef __cplusplus
extern "C" {
#endif

#include "stm32f0xx.h"

// numer I2C:
#define I2Cx I2C1

// ustawienia pinow I2C
#define I2C_SDA_PORT GPIOA
#define I2C_SDA_PIN	 10
#define I2C_SDA_ALTERNATE_FUNCTION_NUMBER 4

#define I2C_SCL_PORT GPIOA
#define I2C_SCL_PIN	 9
#define I2C_SCL_ALTERNATE_FUNCTION_NUMBER 4

// ustawienie zegara dla I2C

#define I2C_CLK_EN 	RCC->APB1ENR |= RCC_APB1ENR_I2C1EN

#define I2C_PINS_CLK_EN RCC->AHBENR |= RCC_AHBENR_GPIOAEN

// makra wykonawcze:
#define I2C_SDA_ALTERNATE GPIO_MODER(I2C_SDA_PORT) |= ( 2 << ( 2 * I2C_SDA_PIN ) )
#if I2C_SDA_PIN < 8
	#define I2C_SDA_ALTERNATE_NR GPIO_AFRL(I2C_SDA_PORT) |= ( I2C_SDA_ALTERNATE_FUNCTION_NUMBER << ( 4 * I2C_SDA_PIN ) )
#else
	#define I2C_SDA_ALTERNATE_NR GPIO_AFRH(I2C_SDA_PORT) |= ( I2C_SDA_ALTERNATE_FUNCTION_NUMBER << ( 4 * ( I2C_SDA_PIN - 8 ) ) )
#endif

#define I2C_SCL_ALTERNATE GPIO_MODER(I2C_SCL_PORT) |= ( 2 << ( 2 * I2C_SCL_PIN ) )

#if I2C_SCL_PIN < 8
	#define I2C_SCL_ALTERNATE_NR GPIO_AFRL(I2C_SCL_PORT) |= ( I2C_SCL_ALTERNATE_FUNCTION_NUMBER << ( 4 * I2C_SCL_PIN ) )
#else
	#define I2C_SCL_ALTERNATE_NR GPIO_AFRH(I2C_SCL_PORT) |= ( I2C_SCL_ALTERNATE_FUNCTION_NUMBER << ( 4 * ( I2C_SCL_PIN - 8 ) ) )
#endif



// uruchomienie i2c
void init_twi( void );

// funkcja dokonujaca zapis pojedynczego znaku
uint8_t TWI_zapis_znak( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t wartosc, uint32_t timeout );

// funkcja wykonujaca zapis bufora
uint8_t TWI_zapis_buff( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t *buff, uint8_t ilosc, uint32_t timeout );

// funkcja odczytujaca pojedynczy znak
uint8_t TWI_odczyt_znak( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t *wsk, uint32_t timeout );

// funkcja odczytujaca bufor danych z i2c
uint8_t TWI_odczyt_buff( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t ilosc_danych, uint8_t *buff, uint32_t timeout );

#ifdef __cplusplus
}
#endif

#endif /* TWI_H_ */
