/*
 * twi.c
 *
 *  Created on: 5 paz 2016
 *      Author: Przemek
 */

#include "twi.h"
#include "DMB/rejestry.h"
#include "stm32f0xx.h"



// uruchomienie i2c na pinach PF6 i PF7
void init_twi( void )
{
	// wlaczenie zegarow
	I2C_PINS_CLK_EN;

	// wlaczenie zegara dla i2c
	I2C_CLK_EN;

//	// wlaczenie syscfgen
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

	// ustawienie pinow

	// tryb alternate
	I2C_SDA_ALTERNATE;
	I2C_SCL_ALTERNATE;

	// przypisanie funkcji alternatywnej
	I2C_SDA_ALTERNATE_NR;
	I2C_SCL_ALTERNATE_NR;

	// wyjscia typu open drain:
	GPIO_OTYPER( I2C_SDA_PORT ) |= ( 1 << I2C_SDA_PIN );
	GPIO_OTYPER( I2C_SCL_PORT ) |= ( 1 << I2C_SCL_PIN );

	// ustawienia i2c:

	// wylaczenie filtru analogowego:
	I2C_CR1( I2Cx ) |= I2C_CR1_ANFOFF;


	// wygenerowane kalkulatorem I2C_Timing_Configuration_V1.0.1
	I2C_TIMINGR( I2Cx ) = ( uint32_t)0x20000A0D;

	// wlaczenie i2c:
	I2C_CR1( I2Cx ) |= I2C_CR1_PE;
}

// funkcja dokonujaca zapis pojedynczego znaku
uint8_t TWI_zapis_znak( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t wartosc, uint32_t timeout )
{
	uint32_t temp_reg;
	uint32_t timeout_counter;
	// test flagi zajetosci
	timeout_counter = 0;
	while( I2C_ISR( I2Cx ) & I2C_ISR_BUSY )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );

	// wyczyszczenie rejestru:
	temp_reg &= ~( I2C_CR2_SADD_Msk | I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD );

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// operacja reload
	temp_reg |= I2C_CR2_RELOAD;

	// wpisanie ile bajtow chcemy przeslac:
	temp_reg |= ( 1 << I2C_CR2_NBYTES_Pos );

	// rozpoczecie transmisji - wyslanie adresu urzadzenia:
	temp_reg |= I2C_CR2_START;

	I2C_CR2( I2Cx ) = temp_reg;

	// czekamy az rejestr wyjsciowy bedzie pusty:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TXIS ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	// wyslanie adresu rejestru:
	I2C_TXDR( I2Cx ) = (uint8_t)adres_rejestru;

	// oczekiwanie na flage TCR:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TCR ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );

	temp_reg &= ~( I2C_CR2_SADD_Msk | I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD );

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// wpisanie ile bajtow chcemy zapisac:
	temp_reg |= ( 1 << I2C_CR2_NBYTES_Pos );

	// auto end mode:
	temp_reg |= I2C_CR2_AUTOEND;

	I2C_CR2( I2Cx ) = temp_reg;

	// czekamy az rejestr wyjsciowy bedzie pusty:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TXIS ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	// wyslanie naszej danej
	I2C_TXDR( I2Cx ) = (uint8_t)wartosc;

	// oczekiwanie na flage stopu:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_STOPF ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	//wyczyszczenie flagi stopu:
	I2C_ICR( I2Cx ) = I2C_ICR_STOPCF;

	// zwrocenie wartosci - poprawnie przeprowadzone operacja
	return 1;
}

// funkcja wykonujaca zapis bufora
uint8_t TWI_zapis_buff( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t *buff, uint8_t ilosc, uint32_t timeout )
{
	uint32_t temp_reg;
	uint8_t licznik = 0;
	uint32_t timeout_counter;

	// test flagi zajetosci
	timeout_counter = 0;
	while( I2C_ISR( I2Cx ) & I2C_ISR_BUSY )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );

	// wyczyszczenie rejestru:
	temp_reg &= ~( I2C_CR2_SADD_Msk | I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD );

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// operacja reload
	temp_reg |= I2C_CR2_RELOAD;

	// wpisanie ile bajtow chcemy przeslac:
	temp_reg |= ( 1 << I2C_CR2_NBYTES_Pos );

	// rozpoczecie transmisji - wyslanie adresu urzadzenia:
	temp_reg |= I2C_CR2_START;

	I2C_CR2( I2Cx ) = temp_reg;

	// czekamy az rejestr wyjsciowy bedzie pusty:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TXIS ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	// wyslanie adresu rejestru:
	I2C_TXDR( I2Cx ) = (uint8_t)adres_rejestru;

	// oczekiwanie na flage TCR:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TCR ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );

	temp_reg &= ~( I2C_CR2_SADD_Msk | I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD );

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// wpisanie ile bajtow chcemy zapisac:
	temp_reg |= ( ilosc << I2C_CR2_NBYTES_Pos );

	// auto end mode:
	temp_reg |= I2C_CR2_AUTOEND;

	I2C_CR2( I2Cx ) = temp_reg;

	while( licznik < ilosc )
	{
		// czekamy az rejestr wyjsciowy bedzie pusty:
		timeout_counter = 0;
		while( !( I2C_ISR( I2Cx ) & I2C_ISR_TXIS ) )
		{
			timeout_counter++;
			if( timeout_counter > timeout )
			{
				return 0;
			}
		}

		// wyslanie naszej danej
		I2C_TXDR( I2Cx ) = (uint8_t)buff[licznik++];
	}

	// oczekiwanie na flage stopu:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_STOPF ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	//wyczyszczenie flagi stopu:
	I2C_ICR( I2Cx ) = I2C_ICR_STOPCF;

	// zwrocenie wartosci - poprawnie przeprowadzone operacja
	return 1;
}

// funkcja odczytujaca pojedynczy znak
uint8_t TWI_odczyt_znak( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t *wsk, uint32_t timeout )
{
	uint32_t temp_reg;
	uint32_t timeout_counter;

	// test flagi zajetosci
	timeout_counter = 0;
	while( I2C_ISR( I2Cx ) & I2C_ISR_BUSY )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );
	// wyczyszczenie rejestru:
	temp_reg &= ~( I2C_CR2_SADD_Msk | I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD | I2C_CR2_STOP);

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// wpisanie ile bajtow chcemy przeslac:
	temp_reg |= ( 1 << I2C_CR2_NBYTES_Pos );

	// rozpoczecie transmisji - wyslanie adresu urzadzenia:
	temp_reg |= I2C_CR2_START;

	I2C_CR2( I2Cx ) = temp_reg;

	// czekamy az rejestr wyjsciowy bedzie pusty:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TXIS ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	// wyslanie adresu rejestru:
	I2C_TXDR( I2Cx ) = (uint8_t)adres_rejestru;

	// oczekiwanie na flage TC:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TC ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );

	temp_reg &= ~( I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD | I2C_CR2_STOP);

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// wpisanie ile bajtow chcemy odczytac:
	temp_reg |= ( 1 << I2C_CR2_NBYTES_Pos );

	// auto end mode:
	temp_reg |= I2C_CR2_AUTOEND;

	// tryb odczytu z i2c
	temp_reg |= I2C_CR2_RD_WRN;

	// rozpoczecie transmisji
	temp_reg |= I2C_CR2_START;

	I2C_CR2( I2Cx ) = temp_reg;

	// oczekiwanie na odbior:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_RXNE ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	*wsk = I2C_RXDR( I2Cx );

	// oczekiwanie na flage stopu:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_STOPF ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	//wyczyszczenie flagi stopu:
	I2C_ICR( I2Cx ) = I2C_ICR_STOPCF;

	// zwrocenie wartosci - poprawnie przeprowadzone operacja
	return 1;
}

// funkcja odczytujaca bufor danych
uint8_t TWI_odczyt_buff( uint8_t adres_urzadzenia, uint8_t adres_rejestru, uint8_t ilosc_danych, uint8_t *buff, uint32_t timeout )
{
	uint8_t licznik = 0;
	uint32_t temp_reg;
	uint32_t timeout_counter;

	// test flagi zajetosci
	timeout_counter = 0;
	while( I2C_ISR( I2Cx ) & I2C_ISR_BUSY )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );
	// wyczyszczenie rejestru:
	temp_reg &= ~( I2C_CR2_SADD_Msk | I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD | I2C_CR2_STOP);

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// wpisanie ile bajtow chcemy przeslac:
	temp_reg |= ( 1 << I2C_CR2_NBYTES_Pos );


	// rozpoczecie transmisji - wyslanie adresu urzadzenia:
	temp_reg |= I2C_CR2_START;

	I2C_CR2( I2Cx ) = temp_reg;

	// czekamy az rejestr wyjsciowy bedzie pusty:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TXIS ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	// wyslanie adresu rejestru:
	I2C_TXDR( I2Cx ) = (uint8_t)adres_rejestru;

	// oczekiwanie na flage TC:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_TC ) )
	{
		timeout_counter++;
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	temp_reg = I2C_CR2( I2Cx );

	temp_reg &= ~( I2C_CR2_RD_WRN | I2C_CR2_NBYTES_Msk | I2C_CR2_AUTOEND | I2C_CR2_RELOAD | I2C_CR2_STOP);

	// wpisanie adresu urzadzenia
	temp_reg |= ( adres_urzadzenia << 1 );

	// wpisanie ile bajtow chcemy odczytac:
	temp_reg |= ( ilosc_danych << I2C_CR2_NBYTES_Pos );

	// auto end mode:
	temp_reg |= I2C_CR2_AUTOEND;

	// tryb odczytu z i2c
	temp_reg |= I2C_CR2_RD_WRN;

	// rozpoczecie transmisji
	temp_reg |= I2C_CR2_START;

	I2C_CR2( I2Cx ) = temp_reg;

	while( licznik < ilosc_danych )
	{
		// oczekiwanie na odbior:
		timeout_counter = 0;
		while( !( I2C_ISR( I2Cx ) & I2C_ISR_RXNE ) )
		{
			timeout_counter++;
			if( timeout_counter > timeout )
			{
				return 0;
			}
		}

		buff[licznik] = I2C_RXDR( I2Cx );
		licznik++;
	}

	// oczekiwanie na flage stopu:
	timeout_counter = 0;
	while( !( I2C_ISR( I2Cx ) & I2C_ISR_STOPF ) )
	{
		if( timeout_counter > timeout )
		{
			return 0;
		}
	}

	//wyczyszczenie flagi stopu:
	I2C_ICR( I2Cx ) = I2C_ICR_STOPCF;

	// zwrocenie wartosci - poprawnie przeprowadzone operacja
	return 1;
}
