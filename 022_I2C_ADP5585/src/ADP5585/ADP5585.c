/*
 * ADP5585.c
 *
 *  Created on: 17.02.2017
 *      Author: Przemek
 */

#include <ADP5585/ADP5585.h>
#include "stm32f0xx.h"
#include "DMB/rejestry.h"
#include "DMB/debug.h"
#include "RCC/RCC.h"
#include "delay_systick/delay_systick.h"
#include "TWI/twi.h"


uint8_t ADP5585_irq_flag = 0;

// Callback dla odebranej linii przez UART
ADP5585_Callback_type *my_ADP5585_callbackk_pointer;

// funkcja rejestrujaca callback
void ADP5585_register_event_callback( ADP5585_Callback_type wsk )
{
	my_ADP5585_callbackk_pointer = wsk;
}

// funkcja sprawdzajaca wystapienie eventu i wywolujaca callback
void ADP5585_event_check( void )
{
	if(ADP5585_irq_flag)
	{
		uint8_t odczyt;
		ADP5585_irq_flag = 0;

		ADP5585_pobierz_event( &odczyt );
		ADP5585_clear_event_irq();

		if(my_ADP5585_callbackk_pointer)
		{
			my_ADP5585_callbackk_pointer( odczyt );
		}
	}
}

void ADP5585_init( void )
{
	init_twi();
	ADP5585_init_1mhz_and_irqcfg();
	ADP5585_uruchom_klawiature_4_4();
	ADP5585_init_key_irq();
	ADP5585_init_exti_irq();
}

// funkcja sprawdzajaca ID z ukladu
// 1 - poprawne, 0 - blad (komunikacji lub adresu)
uint8_t ADP5585_sprawdz_ID( void )
{
	uint8_t odczyt;
	if( TWI_odczyt_znak( ADP5585_ADDRESS, ADP5585_ID, &odczyt, 500000) )
	{
		if( (odczyt & 0xf0 ) == 0x20 )
		{
			return 1;
		}
	}
	return 0;
}

// funkcja sprawdzajaca ID z ukladu
// 0 - blad komunikacji, 1 - ok
uint8_t ADP5585_ilosc_eventow( uint8_t *l_ev )
{
	uint8_t odczyt;

	if( TWI_odczyt_znak( ADP5585_ADDRESS, ADP5585_STATUS, &odczyt, 500000) )
	{
		*l_ev = odczyt & 0x0f;
		return 1;
	}
	return 0;
}

// funkcja sprawdzajaca ID z ukladu
// 0 - blad komunikacji, 1 - ok
uint8_t ADP5585_int_status( uint8_t *l_ev )
{
	uint8_t odczyt;

	if( TWI_odczyt_znak( ADP5585_ADDRESS, ADP5585_INT_STATUS, &odczyt, 500000) )
	{
		*l_ev = odczyt & 0x0f;
		return 1;
	}
	return 0;
}

// funkcja uruchamiajaca klawiature matrycowa 4x4
// 0 - blad komunikacji, 1 - ok
uint8_t ADP5585_uruchom_klawiature_4_4( void )
{
	// wlaczenie skanowania na liniach R0-R3
	if( TWI_zapis_znak(ADP5585_ADDRESS, ADP5585_PIN_CONFIG_A, 0x0f, 500000) )
	{

	}
	else
	{
		return 0;
	}

	// wlaczenie skanowania na liniach C0-C3
	if( TWI_zapis_znak(ADP5585_ADDRESS, ADP5585_PIN_CONFIG_B, 0x0f, 500000) )
	{

	}
	else
	{
		return 0;
	}

	return 1;
}

// funkcja uruchamiajaca wewnetrzne taktowanie
// 0 - blad komunikacji, 1 - ok
uint8_t ADP5585_init_1mhz_and_irqcfg( void )
{
	// wlaczenie skanowania na liniach R0-R3
	if( TWI_zapis_znak(ADP5585_ADDRESS, ADP5585_GENERAL_CFG, 0x82, 500000) )
	{

	}
	else
	{
		return 0;
	}

	return 1;
}

// funkcja uruchamiajaca wewnetrzne taktowanie
// 0 - blad komunikacji, 1 - ok
uint8_t ADP5585_init_key_irq( void )
{
	// wlaczenie skanowania na liniach R0-R3
	if( TWI_zapis_znak(ADP5585_ADDRESS, ADP5585_INT_EN, 0x01, 500000) )
	{

	}
	else
	{
		return 0;
	}

	return 1;
}

// funkcja kasujaca wewnetrzna flage przerwania
// 0 - blad komunikacji, 1 - ok
uint8_t ADP5585_clear_event_irq( void )
{
	// wlaczenie skanowania na liniach R0-R3
	if( TWI_zapis_znak(ADP5585_ADDRESS, ADP5585_INT_STATUS, 0x01, 500000) )
	{

	}
	else
	{
		return 0;
	}

	return 1;
}

// funkcja pobierajaca pierwszy event z fifo
uint8_t ADP5585_pobierz_event( uint8_t* dana )
{
	uint8_t odczyt;

	if( TWI_odczyt_znak( ADP5585_ADDRESS, ADP5585_FIFO_1, &odczyt, 500000) )
	{
		*dana = odczyt;
		return 1;
	}
	return 0;
}

void ADP5585_init_exti_irq( void )
{
	// wlaczenie zegara dla portu A
	RCC->AHBENR |= RCC_AHBENR_GPIOAEN;

	// w rejestrze MODER nie musimy nic ustawiac - domyslnie jest juz wejsciem

	// ustawienie podciagniecia do zasilania
	GPIOA->PUPDR |= GPIO_PUPDR_PUPDR6_0;

	// EXTI kontroluje peryferium o nazwie SYSCFG - standardowo - musimy wlaczyc jego zegar
	RCC->APB2ENR |= RCC_APB2ENR_SYSCFGEN;

	// wylaczamy maskowanie na interesujacy nas pin
	// tzn uruchamiamy jego "obserwowanie"
	EXTI->IMR |= EXTI_IMR_IM6;

	// teraz musimy wybrac z jakiego portu ten pin ma byc - tym zarzadza SYSCFG
	SYSCFG->EXTICR[1] |= SYSCFG_EXTICR2_EXTI6_PA;

	// wybieramy teraz typ zbocza jaki nas interesuje (mozemy oba)
	// dla nas jest to opadajace zbocze
	EXTI->FTSR |= EXTI_FTSR_FT6;

	// teraz uruchamiamy przerwanie w NVIC
	NVIC_EnableIRQ( EXTI4_15_IRQn );
}


// funkcja ktora bedzie sie uruchamiac podczas przerwania:
void EXTI4_15_IRQHandler( void )
{
	// sprawdzamy co wywolalo nasze przerwanie:
	if( EXTI->PR & EXTI_PR_PR6 )
	{
		// kasujemy flage
		EXTI->PR = EXTI_PR_PR6;
		ADP5585_irq_flag = 1;
	}
}
